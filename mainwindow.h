#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onConvert();
    void onCalculateTile();

private:
    Ui::MainWindow *ui;
    void GetInputAsWgs(qreal &inX, qreal &inY);
    void WriteOutputFromWgs(qreal inX, qreal inY);
};

#endif // MAINWINDOW_H
