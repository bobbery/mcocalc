#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mcomath.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->btnConvert, SIGNAL(clicked(bool)), this, SLOT(onConvert()));
    connect(ui->btnTileCalculate, SIGNAL(clicked(bool)), this, SLOT(onCalculateTile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}



enum Formats
{
    FMT_WGS,
    FMT_GdfWGS,
    FMT_NDS,
    FMT_MERC
};





void MainWindow::GetInputAsWgs(qreal &inX, qreal &inY)
{
    quint32 inFmtIndex = ui->cbInputFormat->currentIndex();

    if (inFmtIndex == FMT_WGS)
    {
        inX = ui->leInX->text().toDouble();
        inY = ui->leInY->text().toDouble();
    }
    else if (inFmtIndex == FMT_GdfWGS)
    {
        GdfWGSToWGS(ui->leInX->text().toInt(), ui->leInY->text().toInt(), inX, inY);
    }
    else if (inFmtIndex == FMT_NDS)
    {
        NDSToWGS(inX, inY, ui->leInX->text().toInt(), ui->leInY->text().toInt());
    }
    else if (inFmtIndex == FMT_MERC)
    {
        MercatorToWGS(inX, inY, ui->leInX->text().toInt(), ui->leInY->text().toInt());
    }
}

void MainWindow::WriteOutputFromWgs(qreal inX, qreal inY)
{
    quint32 outFmtIndex = ui->cbOutputFormat->currentIndex();

    if (outFmtIndex == FMT_WGS)
    {
        ui->leOutX->setText(QString::number(inX));
        ui->leOutY->setText(QString::number(inY));
    }
    else if (outFmtIndex == FMT_GdfWGS)
    {
        qint32 outX, outY;
        WGSToGdfWGS(inX, inY, outX, outY);
        ui->leOutX->setText(QString::number(outX));
        ui->leOutY->setText(QString::number(outY));
    }
    else if (outFmtIndex == FMT_NDS)
    {
        qint32 outX, outY;
        WGSToNDS(outX, outY, inX, inY);
        ui->leOutX->setText(QString::number(outX));
        ui->leOutY->setText(QString::number(outY));
    }
    else if (outFmtIndex == FMT_MERC)
    {
        qint32 outX, outY;
        WGSToMercator(outX, outY, inX, inY);
        ui->leOutX->setText(QString::number(outX));
        ui->leOutY->setText(QString::number(outY));
    }
}

void MainWindow::onConvert()
{

    qreal inX = 0.0; // Internal format is WGS
    qreal inY = 0.0;

    GetInputAsWgs(inX, inY);

    WriteOutputFromWgs(inX, inY);

}

void MainWindow::onCalculateTile()
{
    qint32 inX = ui->leTileX->text().toInt();
    qint32 inY = ui->leTileY->text().toInt();
    quint8 level = ui->cbLevel->currentText().toInt();

    quint32 packedTileId = PackFromAbsolutePosition(level, inX, inY);

    ui->lePackedTileId->setText(QString::number(packedTileId));
}
