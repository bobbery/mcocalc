#-------------------------------------------------
#
# Project created by QtCreator 2016-10-02T18:09:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = McoCalc
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mcomath.cpp

HEADERS  += mainwindow.h \
    mcomath.h

FORMS    += mainwindow.ui
