#ifndef MCOMATH_H
#define MCOMATH_H


#include <QtMath>



void WGSToNDS(qint32& x, qint32& y, const qreal wgsX, const qreal wgsY);
void NDSToWGS(qreal& wgsX, qreal& wgsY, const qint32 ndsX, const qint32 ndsY);

void MercatorToWGS(qreal& lon,qreal& lat, const qint32 mercatorX, const qint32 mercatorY);
void WGSToMercator(qint32& mercatorX, qint32& mercatorY, const qreal lon, const qreal lat);

void GdfWGSToWGS(qint32 gdfX, qint32 gdfY, qreal &wgsX, qreal &wgsY);
void WGSToGdfWGS(qreal wgsX, qreal wgsY, qint32 &gdfX, qint32 &gdfY);


quint32 PackFromAbsolutePosition(quint8 level, const qint32 x, const qint32 y);


#endif // MCOMATH_H
