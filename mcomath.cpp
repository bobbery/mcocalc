#include "mcomath.h"

#include <QtGlobal>
#include <QtMath>


namespace Constants
{
    const quint32  EarthRadiusInMeter = 6371000;
    const qreal Pi = 3.1415926535897932384626433832795;
    const qreal rad2deg = 180.0 / Pi;
    const qreal deg2rad = Pi / 180.0;

    const qreal wgs2ndsFactor = 90.0 / 1073741824.0;

    const qint32 MercatorXMax = 20015087;
    const qint32 MercatorXMin = -20015087;
    const qint32 MercatorYMax = 188269393;
    const qint32 MercatorYMin = -188265941;

    const qreal WgsXMax =  180.0;
    const qreal WgsXMin = -180.0;
    const qreal WgsYMax =   90.0;
    const qreal WgsYMin =  -90.0;

    const qint32 NDSXMax = INT_MAX;
    const qint32 NDSXMin = INT_MIN;
    const qint32 NDSYMax =   1 << 30;
    const qint32 NDSYMin = -(1 << 30);

    const qreal floatThreshold = 1e-6;
    const qreal doubleThreshold = 1e-15;

    const qreal xyFactor = 10000000.0;

} // namespace Constants




template <class T>
T Clamp(const T& value, const T& minimum, const T& maximum)
{
    if (value < minimum)
    {
        return minimum;
    }
    if (value > maximum)
    {
        return maximum;
    }
    return value;
}

qreal NDSToWGS(const qint32 nds)
{
    return static_cast<qreal>(nds) * Constants::wgs2ndsFactor;
}

qreal WGSToNDS(const qreal wgs)
{
    const qreal nds = wgs / Constants::wgs2ndsFactor;
    return static_cast<qint32>(qRound(nds));
}

void WGSToNDS(qint32& x, qint32& y, const qreal wgsX, const qreal wgsY)
{
    x = WGSToNDS(wgsX);
    y = WGSToNDS(wgsY);
}

void NDSToWGS(qreal& wgsX, qreal& wgsY, const qint32 ndsX, const qint32 ndsY)
{
    wgsX = NDSToWGS(ndsX);
    wgsY = NDSToWGS(ndsY);
}

void MercatorToWGS(qreal& lon,qreal& lat, const qint32 mercatorX, const qint32 mercatorY)
{
    const qreal mercXf = static_cast<qreal>(mercatorX);
    const qreal mercYf = static_cast<qreal>(mercatorY);
    const qreal atanPart = atan(exp(mercYf / Constants::EarthRadiusInMeter));
    lon = (mercXf * Constants::rad2deg) / Constants::EarthRadiusInMeter;
    lat = (360.0 / Constants::Pi) * (atanPart - Constants::Pi / 4.0);
    lon = Clamp(lon, -180.0, 180.0);
    lat = Clamp(lat,  -90.0,  90.0);
}

void WGSToMercator(qint32& mercatorX, qint32& mercatorY, const qreal lon, const qreal lat)
{
    const qreal radius = static_cast<qreal>(Constants::EarthRadiusInMeter);

    const qreal mercX = radius * Constants::deg2rad * lon;
    mercatorX  = static_cast<qint32>(qRound(mercX));
    const qreal fTan = tan((Constants::Pi / 4.0) + (Constants::Pi / 360.0) * lat);

    if (fTan <= 0.0)
    {
        mercatorY = Constants::MercatorYMin;
    }
    else
    {
        const qreal fLn = log(fTan);
        const qreal mercY = radius * fLn;
        mercatorY = static_cast<qint32>(qRound(mercY));
    }
    mercatorX = Clamp(mercatorX, Constants::MercatorXMin, Constants::MercatorXMax);
    mercatorY = Clamp(mercatorY, Constants::MercatorYMin, Constants::MercatorYMax);
}


void GdfWGSToWGS(qint32 gdfX, qint32 gdfY, qreal &wgsX, qreal &wgsY)
{
    wgsX = gdfX / Constants::xyFactor;
    wgsY = gdfY / Constants::xyFactor;
}

void WGSToGdfWGS(qreal wgsX, qreal wgsY, qint32 &gdfX, qint32 &gdfY)
{
    gdfX = static_cast<qint32>(wgsX * Constants::xyFactor);
    gdfY = static_cast<qint32>(wgsY * Constants::xyFactor);
}



quint32 InterleaveBits(quint16 xIn, quint16 yIn)
{
    quint32 x = static_cast<quint32>(xIn);
    x = (x | (x << 8)) & 0x00FF00FF;
    x = (x | (x << 4)) & 0x0F0F0F0F;
    x = (x | (x << 2)) & 0x33333333;
    x = (x | (x << 1)) & 0x55555555;

    quint32 y = static_cast<quint32>(yIn);
    y = (y | (y << 8)) & 0x00FF00FF;
    y = (y | (y << 4)) & 0x0F0F0F0F;
    y = (y | (y << 2)) & 0x33333333;
    y = (y | (y << 1)) & 0x55555555;

    return x | (y << 1);
}

quint32 Pack(quint8 level, quint32 tileNumber)
{
    return ( 1u << ( level + 16 )) | tileNumber;
}

quint32 PackFromAbsolutePosition(quint8 level, const qint32 x, const qint32 y)
{
    quint16 upperX = static_cast<quint16>( static_cast<quint32>(x) >> 16);
    quint16 upperY = static_cast<quint16>( static_cast<quint32>(y) >> 16);
    quint32 tileNumber = InterleaveBits(upperX, upperY);
    tileNumber = tileNumber & 0x7FFFFFFF;
    tileNumber >>= ( 30 - ( level << 1 ));
    return Pack(level, tileNumber);
}















